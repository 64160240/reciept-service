/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.werapan.databaseproject.componen;

import com.werapan.databaseproject.model.Product;

/**
 *
 * @author Chaiwat
 */
public interface BuyProducttable {
    public void buy(Product product, int qty);
}
